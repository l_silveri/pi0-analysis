#ifndef _PI0CONFIG_H
#define _PI0CONFIG_H

#include <exception>
#include <map>
#include <string>
#include <vector>

class Pi0Config
{
  std::map<std::string, std::string> values;
  std::map<std::string, std::vector<float>> binning;

  void getBinning();

public:
  Pi0Config(){};
  Pi0Config(const char* fname);
  void parseFile(const char* fname);
  std::string getValue(std::string key);
  std::vector<float>& getBinning(std::string varName);
};


class FileNonExisting : public std::exception
{
  const char* what() const noexcept
  {
    return "Can't open file\n";
  }
};

#endif //def _PI0CONFIG_H
