#ifndef _RETRIEVE_DATA_ABS_H
#define _RETRIEVE_DATA_ABS_H

#include <map>
#include <vector>
#include <string>
#include "TChain.h"
#include "TFile.h"

#define N_PARTICLES 2
#define N_TOWERS    2

typedef struct
{
  int code[N_TOWERS][N_PARTICLES];
  double p[4][N_TOWERS][N_PARTICLES];
  double pos[3][N_TOWERS][N_PARTICLES];
} MCEvent;

class RetrieveDataAbs
{
protected:
  int nhits[2];
  double energy[2];
  double posx[2];
  double posy[2];
  double x[2];
  double y[2];
  double pid[2];
  double beamcenter[2];
  double lhctodetx[2];
  double lhctodety[2];
  double lor_amp[2];
  double lor_wdt[2];
  std::vector<std::string> files;

  std::map<std::string, std::vector<double>*> v_values;
  std::vector<int>* vrunnumber_;
  std::vector<int>* vmaxposdetlayer_;
  std::vector<int>* vcode_;
  std::vector<unsigned>* vtime_;
  std::vector<unsigned>* vflag_;
  std::vector<bool>* vmultihit_;
  std::vector<bool>* vtrigger_;
  std::vector<bool>* vsisaturated_;


  TChain* format_tree;
  TChain* recons_tree;

  virtual void resetMembers();
  virtual void initialize()=0;
  virtual bool retrieveEvent(int iEv)=0;

public:
  virtual void setupMontecarlo()=0;
  void setFileList(std::vector<std::string>& file_list);
  void setBeamCenter(double x, double y);
  virtual MCEvent* getMCdata()=0;

  friend void cutData(RetrieveDataAbs& data, bool isMontecarlo);

  int getNentries();

};



#endif //def _RETRIEVE_DATA_ABS_H
