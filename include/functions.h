#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H

#include "Pi0Config.h"
#include <string>

#define Y_MIN_BIN_STEP 0.2
#define PT_MIN_BIN_STEP 0.1

class RetrieveDataAbs;



#define N_BINS			0
#define RANGE_MIN		1
#define RANGE_MAX		2


extern Pi0Config conf;

void cutData(RetrieveDataAbs& data, bool isMontecarlo);
void sim_accettanza();
void analyze();
void analyze_mc();
double L90Boundary(int tower, double energy);
std::string float_to_str(float val, int dec_places=1);




#endif //_FUNCTIONS_H
