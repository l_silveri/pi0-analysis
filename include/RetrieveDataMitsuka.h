#ifndef _RETRIEVE_DATA_MITSUKA_H
#define _RETRIEVE_DATA_MITSUKA_H

#include "RetrieveDataAbs.h"

class RetrieveDataMitsuka : public RetrieveDataAbs
{
  void initialize();
  bool retrieveEvent(int iEv);

public:
  RetrieveDataMitsuka();
  RetrieveDataMitsuka(std::vector<std::string>& file_list);
  ~RetrieveDataMitsuka();

  void setupMontecarlo();
  MCEvent* getMCdata();


};


#endif //def _RETRIEVE_DATA_MITSUKA_H
