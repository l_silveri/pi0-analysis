#ifndef _RETRIEVE_DATA_H
#define _RETRIEVE_DATA_H

#include "RetrieveDataAbs.h"
#include "LHCfEvent.hh"

class RetrieveData : public RetrieveDataAbs
{
  int treeElem;
  nLHCf::LHCfEvent* lhcf_ev;

  void initialize();
  bool retrieveEvent(int iEv);

public:
  RetrieveData();
  RetrieveData(std::vector<std::string>& file_list);
  ~RetrieveData();

  void setupMontecarlo();
  MCEvent* getMCdata();


};



#endif //def _RETRIEVE_DATA_H
