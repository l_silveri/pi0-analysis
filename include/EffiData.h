#ifndef _EFFI_DATA_H
#define _EFFI_DATA_H

class TH1F;
class TH2F;

class EffiData
{
protected:
  TH2F** histos;
  char* file_name;
  virtual void updateHistos()=0;

public:
  void setFile(const char* fname);
  bool isSetFile();

  virtual float getValue(float y, float pt)=0;
  virtual TH1F* getPtHistogram(float y, float pmin, float pmax)=0;
}












#endif //def _EFFI_DATA_H
