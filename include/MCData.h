#ifndef _MCDATA_H_
#define _MCDATA_H_

class TH2F;
class TH1F;

class MCData
{
  TH2F* sim_histo_rec;
  TH2F* sim_histo_recpi;
  TH2F* sim_histo_all;
  TH2F* sim_histo_true;
  char* mc_fname;

  void updateHistos();

public:
  MCData();
  MCData(const char* fname);
  ~MCData();

  void setFile(const char* fname);
  bool isSetFile();
  void selectEventType(int eventType);

  float getCorrectionError(float y, float pt);
  float getValue(float y, float pt);
  float getBkgCorrection(float y, float pt);
  TH1F* getPtHistogram(float y, float pmin, float pmax);
};











#endif //def _MCDATA_H_
