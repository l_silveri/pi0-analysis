#ifndef _ACCEPTANCE_DATA_H
#define _ACCEPTANCE_DATA_H

class TH1F;
class TH2F;

class AcceptanceData
{
    char* sim_file_name;
    TH2F* sim_data_t[2];
    TH2F* sim_tot;
    int evType;
    void updateHistos();

public:
    AcceptanceData();
    AcceptanceData(const char* fname);
    ~AcceptanceData();

    void setFile(const char* fname);
    bool isSetFile();
    void selectEventType(int eventType);

    float getValue(float y, float pt);
    TH1F* getPtHistogram(float y, float pmin, float pmax);
};




#endif //def    _ACCEPTANCE_DATA_H
