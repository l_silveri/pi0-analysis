CFLAGS=`root-config --cflags` -Iinclude/
LDFLAGS=`root-config --libs` -lRooFit -lHtml -lMinuit -lRooFitCore -lstdc++fs

# link new library (Alessio)
NEW_SOFTWARE_DIR = ../Library/
NEW_BUILD_DIR = build
NEW_INC_DIR = ${NEW_SOFTWARE_DIR}/Dictionary/include
NEW_LIB_DIR = ${NEW_SOFTWARE_DIR}/${NEW_BUILD_DIR}/lib
CFLAGS  += -I${NEW_INC_DIR}
LDFLAGS += -L${NEW_LIB_DIR} -lDictionary -Wl,-rpath=${NEW_LIB_DIR}

all:
	g++ -c src/AcceptanceData.cpp ${CFLAGS} -o objs/AcceptanceData.o
	g++ -c src/MCData.cpp ${CFLAGS} -o objs/MCData.o
	g++ -c src/RetrieveDataAbs.cpp  ${CFLAGS} -o objs/RetrieveDataAbs.o
	g++ -c src/RetrieveDataMitsuka.cpp  ${CFLAGS} -o objs/RetrieveDataMitsuka.o
	g++ -c src/RetrieveData.cpp ${CFLAGS} -o objs/RetrieveData.o
	g++ -c src/cuts.cpp  ${CFLAGS} -o objs/cuts.o
	g++ -c src/analyze.cpp -o objs/analyze.o  ${CFLAGS}
	g++ -c src/analyze_mc.cpp -o objs/analyze_mc.o ${CFLAGS}
	g++ -c src/Pi0Config.cpp -o objs/Pi0Config.o ${CFLAGS}
	g++ -c src/sim_accettanza.cpp -o objs/sim_accettanza.o ${CFLAGS}
	g++ -o pi0_run src/main.cpp objs/*.o ${CFLAGS} ${LDFLAGS}


clean:
	rm pi0_run objs/*.o
