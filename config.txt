# Pi0 analysis config file
# Comments start with '#'
# Commands are still under costruction
# on/off/auto MUST BE lowercase, otherwise default value should be apply



# ----------------------------------------------------- #
# ------------ FILES SETTINGS ------------------------- #
# ----------------------------------------------------- #

# Specify Montecarlo data location and output
montecarlo_dir = montecarlo/
montecarlo_cut = results/montecarlo_res.root
montecarlo_hst = results/pt_histos_mc.root

# Specify Run data location and output
run_dir = run_data/
run_cut = results/data_res.root
run_hst = results/pt_histos.root

# Specify Geometrical Acceptance file location
acc_hst = results/y_pt_histos_sim.root



# ----------------------------------------------------- #
# ------------ CUT SETTINGS --------------------------- #
# ----------------------------------------------------- #

# Choose if apply cuts, default = off
apply_cuts = off

# Choose cut on L90%, choose auto or the value in X0 units
# t1 for small tower, t2 for large tower, default is auto
l90_cut_t1 = auto
# l90_cut_t1 = 20
l90_cut_t2 = auto
# l90_cut_t2 = 20

# if auto, choose single photon selection efficiency
# 0 -> 90%, 1 -> 85%, 2 -> 95%
l90_sel_eff_mode = 0

# Choose cut on border (mm)
border_size = 2

# Choose cut on photon energy (GeV units)
energy_ph_t1 = 200
energy_ph_t2 = 200
# Multiplier of photon energy (Error calculation purposes)
ph_energy_factor = 1

# Insert beamcenter position in mm for Montecarlo data
beamcenterx_mc = -3.272
beamcentery_mc = -2.959

# Insert beamcenter position in mm for Physics data
beamcenterx_data = -3.488
beamcentery_data = -2.706



# ----------------------------------------------------- #
# ------------ BIN SETTINGS --------------------------- #
# ----------------------------------------------------- #

# TODO: custom binning
# Set rapidity and transverse momentum binning, list of extremes of the interval, minimum step is 0.1
# yBins = 9.2, 9.4, 9.6, 9.8, 10.0, 10.6
#ptBins = 0, 0.1, 0.2, 0.3, 0.4, 0.6, 1.0

# This is actually working ( nBins, inf, sup)
yBins = 7, 9.2, 10.6
ptBins = 10, 0, 1



# ----------------------------------------------------- #
# ------------ ANALYSIS ROUTINES----------------------- #
# ----------------------------------------------------- #

# Set analysis with type II pi0 events (not working), default=off
typeII = off

# Do simulation for acceptance with acceptance_iterations iterations
# with defined intervals for y and pt
acceptance_sim = off
acceptance_iterations = 1 000 000 000
acceptance_y_interval = 7, 15
acceptance_pt_interval = 0, 3


# Re-elaborate Montecarlo simulations datasets analysis
# Requires option on when changing binning set
montecarlo_analysis = on

# Perform data analysis, unless you are debugging should leave it on
data_analysis = off
