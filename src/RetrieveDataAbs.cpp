#include "RetrieveDataAbs.h"

int RetrieveDataAbs::getNentries()
{
    return recons_tree->GetEntries();
}


void RetrieveDataAbs::setBeamCenter(double x, double y)
{
    beamcenter[0] = x;
    beamcenter[1] = y;
}


void RetrieveDataAbs::setFileList(std::vector<std::string>& file_list)
{
    this->files = file_list;

    if (files.size() > 0)
    {
        initialize();
    }
}

void RetrieveDataAbs::resetMembers()
{
    lhctodetx[0] = 8;
    lhctodetx[1] = 41.8;
    lhctodety[0] = 12.5;
    lhctodety[1] = -14.3;
    beamcenter[0] = 0;
    beamcenter[1] = 0;


    vrunnumber_ = nullptr;
    vmaxposdetlayer_ = nullptr;
    vtime_ = nullptr;
    vflag_ = nullptr;
    vmultihit_ = nullptr;
    vtrigger_ = nullptr;
    vsisaturated_ =nullptr;
    format_tree = nullptr;
    recons_tree = nullptr;

}
