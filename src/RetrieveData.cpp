#include "RetrieveData.h"
#include "Arm2RecPars.hh"
#include "Level3.hh"
#include "McEvent.hh"
#include "ParticleCode.h"

RetrieveData::RetrieveData()
{
    resetMembers();
}

RetrieveData::RetrieveData(std::vector<std::string>& file_list)
{
    resetMembers();
    files = file_list;

    if (files.size() > 0)
    {
        initialize();
    }
}


RetrieveData::~RetrieveData()
{
    // for (auto& v : v_values)
    // {
    //       if (v.second != nullptr) delete v.second;
    // }
    //
    // if (vrunnumber_!=nullptr )  delete vrunnumber_;
    // if (vmaxposdetlayer_!=nullptr ) delete vmaxposdetlayer_;
    // if (vtime_ !=nullptr ) delete vtime_;
    // if (vflag_!=nullptr ) delete vflag_;
    // if (vmultihit_!=nullptr ) delete vmultihit_;
    // if (vtrigger_ != nullptr) delete vtrigger_;
    // if (vsisaturated_!=nullptr ) delete vsisaturated_;
    // if (vcode_ != nullptr ) delete vcode_;
    //
    // if (format_tree != nullptr) delete format_tree;
    // if (recons_tree != nullptr && !new_library) delete recons_tree;
    // if (lhcf_ev != nullptr) delete lhcf_ev;
}


void RetrieveData::initialize()
{
  recons_tree = new TChain("LHCfEvents");
  //recons_tree->SetCacheSize(10000000);
  format_tree = recons_tree;

  for ( std::string s : files )
  {
    recons_tree->Add(s.c_str());
  }
  lhcf_ev = new nLHCf::LHCfEvent("event", "LHCfEvent");
  recons_tree->SetBranchAddress("ev.", &lhcf_ev);
  //recons_tree->AddBranchToCache("*");
}


void RetrieveData::setupMontecarlo()
{
  std::string names[] = { "posx", "posy", "posz", "ekin", "px", "py", "pz"};

  if ( format_tree != nullptr )
  {
    for ( std::string s : names )
    {
      v_values[s] = new std::vector<double>;
    }
  }

  vcode_ = new std::vector<int>();
}


bool RetrieveData::retrieveEvent(int iEv)
{
  bool entry_flag = false;

  if (iEv != 0)
  {
    lhcf_ev->HeaderClear();
    lhcf_ev->ObjDelete();
  }
  entry_flag = recons_tree->GetEntry(iEv);

  if ( entry_flag )
  {
    nLHCf::Level3<nLHCf::Arm2RecPars> *lvl3 = (nLHCf::Level3<nLHCf::Arm2RecPars> *)lhcf_ev->Get("lvl3_a2");
    for (int iT = 0; iT < 2; iT++)
    {
      pid[iT]     = lvl3->fPhotonL90[iT];
      //energy[iT]  = lvl3->fPhotonEnergy[iT];    // without leakage-in correction
      energy[iT]  = lvl3->fPhotonEnergyLin[iT]; // with leakage-in correction
      posx[iT]    = lvl3->fPhotonPosition[iT][0];
      posy[iT]    = lvl3->fPhotonPosition[iT][1];
      x[iT]       = -(posx[iT] - lhctodetx[iT] - beamcenter[0]);
      y[iT]       = posy[iT] - lhctodety[iT] - beamcenter[1];
      //Get Multihit flag: 0 if not triggered, 1 for a single hit, 2 for multihit
      nhits[iT]   = (lvl3->fPhotonTrigger[iT] ? 1 : 0) * (lvl3->fPhotonMultiHit[iT] ? 2 : 1);

      int max_x   = lvl3->fPosMaxLayer[iT][0];
      int max_y   = lvl3->fPosMaxLayer[iT][1];
      lor_amp[iT] = (lvl3->fPhotonPosFitPar[iT][max_x][0][0] + lvl3->fPhotonPosFitPar[iT][max_y][1][0]) / 2.;
      double lor_wdt_x[3] = {lvl3->fPhotonPosFitPar[iT][max_x][0][3],
			            lvl3->fPhotonPosFitPar[iT][max_x][0][5],
			            lvl3->fPhotonPosFitPar[iT][max_x][0][6]};
      double lor_wdt_y[3] = {lvl3->fPhotonPosFitPar[iT][max_x][1][3],
    			        lvl3->fPhotonPosFitPar[iT][max_x][1][5],
			            lvl3->fPhotonPosFitPar[iT][max_x][1][6]};
	    double lor_frac_x[3] = {lvl3->fPhotonPosFitPar[iT][max_x][0][2],
          				lvl3->fPhotonPosFitPar[iT][max_x][0][4],
				          1. - lvl3->fPhotonPosFitPar[iT][max_x][0][2] - lvl3->fPhotonPosFitPar[iT][max_x][0][4]};
      double lor_frac_y[3] = {lvl3->fPhotonPosFitPar[iT][max_x][1][2],
                  lvl3->fPhotonPosFitPar[iT][max_x][1][4],
				          1. - lvl3->fPhotonPosFitPar[iT][max_x][1][2] - lvl3->fPhotonPosFitPar[iT][max_x][1][4]};
	      lor_wdt[iT] = 0.; // TO DO !!!
    }
  }

  return entry_flag;
}



//Note: all lengths are measured in mm
MCEvent* RetrieveData::getMCdata()
{
    MCEvent* mce = new MCEvent;

    const double SiLayerDepth[4][2] = {
      {  1.99,     0.00},
      { 37.02,    35.04},
      {103.71,    69.33},
      {177.69, 139.76}
    };

    const double lhcf_to_layer0 = 46.66;
    double lhcx, lhcy, detx, dety;
    double lhcz = -1127;
    double depth_x = lhcf_to_layer0 + SiLayerDepth[0][0]; // depth of X silicon detector
    double depth_y = lhcf_to_layer0 + SiLayerDepth[0][1]; // depth of Y silicon detector
    int index1[2][2] = { {-1, -1}, {-1, -1}};

    // Convert McEvent to v_values

      nLHCf::McEvent *mc_event = (nLHCf::McEvent *)lhcf_ev->Get("true_a2");

      v_values["posx"]->clear();
      v_values["posy"]->clear();
      v_values["px"]->clear();
      v_values["py"]->clear();
      v_values["pz"]->clear();
      v_values["ekin"]->clear();
      vcode_->clear();

      int size = mc_event->GetN();
      for (int iP = 0; iP < size; iP++)
      {
        	nLHCf::McParticle *part = mc_event->Get(iP);
        	v_values["posx"]->push_back(part->X());
        	v_values["posy"]->push_back(part->Y());
        	v_values["px"]->push_back(part->MomentumX());
        	v_values["py"]->push_back(part->MomentumY());
        	v_values["pz"]->push_back(part->MomentumZ());
        	v_values["ekin"]->push_back(part->Energy());
        	int code, subcode, charge;
        	ParticleCode(&code, &subcode, &charge, part->PdgCode());
        	vcode_->push_back(code);
      }


    if (vcode_ != nullptr)
    {
      int size = vcode_->size();
      //Use index to sort particles by energy_
      for (int iP = 0; iP < size; iP++)
      {
        //Convert position to mm and subtract offset
        lhcx = v_values["posx"]->at(iP)*10;
        lhcy = v_values["posy"]->at(iP)*10 + 20 ;

        // propagate true MC x,y to detector depth
        lhcx += (depth_x - lhcz) * v_values["px"]->at(iP) / v_values["pz"]->at(iP);
        lhcy += (depth_y - lhcz) * v_values["py"]->at(iP) / v_values["pz"]->at(iP);

        int hitTow = lhcy < 13.4 ? 0 : 1; //y=13.4 line separates the 2 towers
        detx = lhcx + lhctodetx[hitTow];
        dety = lhcy + lhctodety[hitTow];

        //if the particle actually hit the tower
        if ( 2 < detx && detx < 23+hitTow*7 &&
             2 < dety && dety < 23+hitTow*7)
        {
          if (index1[hitTow][0] < 0 )
          {
            index1[hitTow][0] = iP;
          }
          else if ( index1[hitTow][1] < 0 )
          {
            if (v_values["ekin"]->at(iP) > v_values["ekin"]->at(index1[hitTow][0]) )
            {
              index1[hitTow][1] = index1[hitTow][0];
              index1[hitTow][0] = iP;
            }
            else
            {
              index1[hitTow][1] = iP;
            }
          }
          else
          {
            bool set_ind = false;
            for (int sP=0; sP<N_PARTICLES && !set_ind; sP++)
            {
              if (v_values["ekin"]->at(iP) > v_values["ekin"]->at(index1[hitTow][sP]) )
              {
                for (int tP=N_PARTICLES-1; tP > sP; tP--)
                {
                  index1[hitTow][tP] = index1[hitTow][tP-1];
                }
                index1[hitTow][sP] = iP;
                set_ind = true;
              }
            }
          }
        }
      }

      for (int iT = 0; iT < N_TOWERS; iT++)
      {
        for (int iP = 0; iP < N_PARTICLES; iP++)
        {
          if (index1[iT][iP] >= 0)
          {

            mce->code[iT][iP] = vcode_->at(index1[iT][iP]);

            mce->p[0][iT][iP] = v_values["ekin"]->at(index1[iT][iP]);
            mce->p[1][iT][iP] = v_values["px"]->at(index1[iT][iP]);
            mce->p[2][iT][iP] = v_values["py"]->at(index1[iT][iP]);
            mce->p[3][iT][iP] = v_values["pz"]->at(index1[iT][iP]);

            mce->pos[0][iT][iP] = v_values["posx"]->at(index1[iT][iP])*10 - beamcenter[0];
            mce->pos[0][iT][iP] += (depth_x - lhcz) * v_values["px"]->at(index1[iT][iP]) / v_values["pz"]->at(index1[iT][iP]);
            mce->pos[1][iT][iP] = v_values["posy"]->at(index1[iT][iP])*10 + 20 - beamcenter[1];
            mce->pos[1][iT][iP] += (depth_y - lhcz) * v_values["py"]->at(index1[iT][iP]) / v_values["pz"]->at(index1[iT][iP]);
            mce->pos[2][iT][iP] = (depth_x + depth_y) / 2.;
          }
          else
          {
            mce->code[iT][iP] = 0;

            mce->p[0][iT][iP] = 0;
            mce->p[1][iT][iP] = 0;
            mce->p[2][iT][iP] = 0;
            mce->p[3][iT][iP] = 0;

            mce->pos[0][iT][iP] = 0;
            mce->pos[1][iT][iP] = 0;
            mce->pos[2][iT][iP] = 0;

          }
        }
      }
    }

    return mce;
}
