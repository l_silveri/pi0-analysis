#include <iostream>
#include "RooFit.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooBifurGauss.h"
#include "RooAddPdf.h"
#include "RooChebychev.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "TH1F.h"
#include "MCData.h"
#include "AcceptanceData.h"
#include <string>
#include <fstream>
#include "functions.h"
using namespace std;
using namespace RooFit;

#define N_BINS          0
#define RANGE_MIN       1
#define RANGE_MAX       2


// int main (int argc, char** argv)
// {
//  TApplication app("app", &argc, argv);
//
//  analyze();
//
//  app.Run();
//  return 0;
// }


void analyze ()
{
    //Prende dati dal tree
    TFile* in_file = NULL;
    in_file = TFile::Open(conf.getValue("run_cut").c_str());
    if (in_file == NULL )
    {
        std::cerr << "File non trovato" << std::endl;
        std::exit(-1);
    }

    TTree* tree = (TTree*) in_file->Get("data_tree");
    RooRealVar mgg("mgg", "mgg", 0, 500, "MeV");
    mgg.setBins(500);
    RooRealVar pi_energy("pi_energy", "pi_energy", 0, 10.e3, "GeV");

    vector<float> y_binning = conf.getBinning("yBins");
    RooRealVar y("y", "rapidity", y_binning[RANGE_MIN], y_binning[RANGE_MAX]);
    y.setBins(y_binning[N_BINS]);

    vector<float> pt_binning = conf.getBinning("ptBins");
    RooRealVar pt("pt", "p transverse", pt_binning[RANGE_MIN], pt_binning[RANGE_MAX], "GeV");
    pt.setBins(pt_binning[N_BINS]);

    RooRealVar px("px", "p, x projection", 0, 1., "GeV");
    RooRealVar py("py", "p, y projection", 0, 1., "GeV");
    RooRealVar pz("pz", "p, z projection", 0, 10.e3, "GeV");
    RooDataSet data("data", "datas from tree", tree, RooArgSet(mgg, pi_energy, y, px, py, pz, pt));
    RooDataSet* bkg;

    AcceptanceData adt(conf.getValue("acc_hst").c_str());
    MCData mcdt(conf.getValue("montecarlo_hst").c_str());

    //Definizione chebychev
    RooRealVar p0("p0", "p0", 0), p1("p1", "p1", 0), p2("p2", "p2", 0);
    RooChebychev cheby("cheby", "chebychev", mgg, RooArgList(p0, p1, p2));

    //Definizione gaussiana asimmetrica
    RooRealVar mu("mu", "mu", 1.40440e+02, 130, 150);
    RooRealVar sigmaL("sigmaL", "sigma left", 6, 2, 8);
    RooRealVar sigmaR("sigmaR", "sigma right", 6, 2, 8);
    RooBifurGauss gs("gauss", "gauss", mgg, mu, sigmaL, sigmaR);

    //Definizione somma delle 2 pdf
    RooRealVar fract("fract", "fraction of ", 0, 1);

    TH1F* sig_hist;
    TH1F* sib_hist;
    TH1F* mgg_hist;
    TH1F* bkg_hist;
    TH1F* mcc_hist;
    TH1F* fin_hist;

    RooAbsData* data_reduced;
    RooAbsData* bkg_data;
    RooAbsData* sig_data;

    TFile* out_file = new TFile(conf.getValue("run_hst").c_str(), "recreate");
    mgg.setRange("fitRange", 70, 210);

    float y_step = ( y_binning[RANGE_MAX] - y_binning[RANGE_MIN] )/(y_binning[N_BINS]);

    //Fit gauss+chebychev
    for (int i=1; i<y_binning[N_BINS]; i++)
    {
        float y_inf, y_sup;

        y_inf = y_binning[RANGE_MIN] + y_step*(i-1);
        y_sup = y_binning[RANGE_MIN] + y_step*i;

        cout << "Starting analysis of bin " << y_inf << " < y < " << y_sup << endl;

        string pdfname = "mgg_pdf_" + to_string(i);
        RooAddPdf pdf(pdfname.c_str(), "cheby + gauss", gs, cheby,fract);
        //Set background fit parameters to 0
        for (RooRealVar* param : {&p0, &p1, &p2, &fract } )
        {
            param->setVal(1);
        }
        for (RooRealVar* param : {&sigmaL, &sigmaR} )
        {
            param->setVal(6);
        }

        y.setRange("y_range", y_inf, y_sup);
        data_reduced = data.reduce(CutRange("y_range"));
        pdf.fitTo(*data_reduced, Range("fitRange"), Minos(kTRUE), PrintLevel(-1), NumCPU(2));
        RooPlot* mggframe = mgg.frame(RooFit::Title(("mgg fit "+to_string(i)).c_str()));
        data_reduced->plotOn(mggframe);
        pdf.paramOn(mggframe);
        pdf.plotOn(mggframe);
        pdf.plotOn(mggframe, RooFit::Components("cheby"), RooFit::LineColor(kGreen+1));
        pdf.plotOn(mggframe, RooFit::Components("gauss"), RooFit::LineColor(kRed));

        //Limite sinistro (limitL) e destro (limitR) per le varie finestre spettrali
        //Finestra di segnale
        Double_t limitLs = mu.getValV() - 3.3*sigmaL.getValV();
        Double_t limitRs = mu.getValV() + 3.3*sigmaR.getValV();
        //Finestra di rumore sinistra
        Double_t limitLb_l = mu.getValV() - 6.6*sigmaL.getValV();
        Double_t limitRb_l = mu.getValV() - 3.3*sigmaL.getValV();
        //Finestra di rumore destra
        Double_t limitLb_r = mu.getValV() + 3.3*sigmaR.getValV();
        Double_t limitRb_r = mu.getValV() + 6.6*sigmaR.getValV();

        mgg.setRange("mgg_range_C", limitLs, limitRs);
        mgg.setRange("mgg_range_L", limitLb_l, limitRb_l);
        mgg.setRange("mgg_range_R", limitLb_r, limitRb_r);



        mgg_hist = new TH1F( ("mgg_hist_"+to_string(i)).c_str(),
                                             ("mgg, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)).c_str(),
                                                100, 0, 500);

        sib_hist = new TH1F( ("sb_hist_"+to_string(i)).c_str(),
                                                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                                                    " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        bkg_hist = new TH1F( ("bkg_hist_"+to_string(i)).c_str(),
                                                 ("bkg_pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                                                    " & "+float_to_str(limitLs) + "  mgg    " + float_to_str(limitRs)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        sig_hist = new TH1F( ("sig_hist_"+to_string(i)).c_str(),
                                                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup) +
                                                    " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        mcc_hist = new TH1F( ("mcc_hist_"+to_string(i)).c_str(),
                                                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup) +
                                                    " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        fin_hist = new TH1F( ("fin_hist_"+to_string(i)).c_str(),
                                                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup) +
                                                    " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);


        RooAbsReal* bgl = cheby.createIntegral(mgg, Range("mgg_range_L"));
        RooAbsReal* bgr = cheby.createIntegral(mgg, Range("mgg_range_R"));
        RooAbsReal* bgc = cheby.createIntegral(mgg, Range("mgg_range_C"));

        Double_t R = bgc->getVal()/(bgl->getVal()+bgr->getVal());
        cout << "bgc is " << bgc->getVal() << endl << "bgl is " << bgl->getVal() << endl
                << "bgr is " << bgr->getVal() << endl;
        cout << "R param for y in (" << y_inf << ", " << y_sup << ") is " << R << endl;

        data_reduced->fillHistogram(mgg_hist, mgg);
        sig_data = data_reduced->reduce(CutRange("mgg_range_C"));
        bkg_data = data_reduced->reduce(CutRange("mgg_range_L"), CutRange("mgg_range_R"));

        sig_data->fillHistogram(sib_hist, pt);
        bkg_data->fillHistogram(bkg_hist, pt);
        sig_data->fillHistogram(sig_hist, pt);
        sig_data->fillHistogram(fin_hist, pt);

        cout << "Background subtraction bin " << y_inf << " < y < " << y_sup << endl;

        float pt_step = (pt_binning[RANGE_MAX] - pt_binning[RANGE_MIN])/pt_binning[N_BINS];

        for (int i=1; i<=pt_binning[N_BINS]; i++)
        {
            float pt_inf = pt_binning[RANGE_MIN] + i*pt_step;
            float val = bkg_hist->GetBinContent(i);
            float corr = 1;//mcdt.getBkgCorrection(y_inf + y_step/2, pt_inf + pt_step/2);
            float err = bkg_hist->GetBinError(i);

            bkg_hist->SetBinContent(i, val*corr);
            //Just for trial: this error should be asymmetric since the correction makes the error one-sided
            bkg_hist->SetBinError(i, sqrt(err*err + val*(1-corr)*val*(1-corr)));
        }
        sig_hist->Add(bkg_hist, -R);

        int nBins = sig_hist->GetNbinsX();

        cout << "Efficiency correction bin " << y_inf << " < y < " << y_sup << endl;

        for (int i=1; i<=nBins; i++)
        {
            Double_t val = sig_hist->GetBinContent(i);
            sig_hist->SetBinError(i, sqrt(sib_hist->GetBinContent(i) + R*R*bkg_hist->GetBinContent(i)));
            Double_t err;
            Double_t acc = adt.getValue(y_inf+y_step/2, sig_hist->GetBinCenter(i));
            Double_t mceffi = mcdt.getValue(y_inf, sig_hist->GetBinCenter(i) - sig_hist->GetBinWidth(i)/2);
            Double_t mcerro = mcdt.getCorrectionError(y_inf, sig_hist->GetBinCenter(i) - sig_hist->GetBinWidth(i)/2);

            if (acc > 0 &&  mceffi > 0)
            {
                mcc_hist->SetBinContent(i,  val/mceffi);
                fin_hist->SetBinContent(i, val/mceffi/acc);
                err = sqrt(sib_hist->GetBinContent(i) + R*R*bkg_hist->GetBinContent(i))/mceffi;
            }
            else
            {
                mcc_hist->SetBinContent(i, 0);
                fin_hist->SetBinContent(i, 0);
                err = 0;
            }
            mcc_hist->SetBinError(i, err);
            fin_hist->SetBinError(i, err/acc);
        }

        mggframe->Write();
        mgg_hist->Write();
        sib_hist->Write();
        sig_hist->Write();
        bkg_hist->Write();
        mcc_hist->Write();
        fin_hist->Write();
        out_file->Flush();

        cout << "Done analysis of bin " << y_inf << " < y < " << y_sup << endl;

    }
    delete in_file;
    delete out_file;
}
