#include "RetrieveDataMitsuka.h"

RetrieveDataMitsuka::RetrieveDataMitsuka()
{
    resetMembers();
}

RetrieveDataMitsuka::RetrieveDataMitsuka(std::vector<std::string>& file_list)
{
    resetMembers();
    files = file_list;

    if (files.size() > 0)
    {
        initialize();
    }
}



RetrieveDataMitsuka::~RetrieveDataMitsuka()
{
    // for (auto& v : v_values)
    // {
    //       if (v.second != nullptr) delete v.second;
    // }
    //
    // if (vrunnumber_!=nullptr )  delete vrunnumber_;
    // if (vmaxposdetlayer_!=nullptr ) delete vmaxposdetlayer_;
    // if (vtime_ !=nullptr ) delete vtime_;
    // if (vflag_!=nullptr ) delete vflag_;
    // if (vmultihit_!=nullptr ) delete vmultihit_;
    // if (vtrigger_ != nullptr) delete vtrigger_;
    // if (vsisaturated_!=nullptr ) delete vsisaturated_;
    // if (vcode_ != nullptr ) delete vcode_;
    //
    // if (format_tree != nullptr) delete format_tree;
    // if (recons_tree != nullptr && !new_library) delete recons_tree;
    // if (lhcf_ev != nullptr) delete lhcf_ev;
}


void RetrieveDataMitsuka::initialize()
{
  format_tree = new TChain("Format");
  recons_tree = new TChain("Reconstruction");


  for ( std::string s : files )
  {
    format_tree->Add(s.c_str());
    recons_tree->Add(s.c_str());
  }

  std::string branch_names [] = { "energy", "pid", "vertex0", "vertex1", "vertex2", "vertex3",
                                "vertex4"/*, "vertex5", "vertex6", "vertex7", "vertex8", "vertex9", "vertex10",
                                "vertexerror0", "vertexerror1", "cal_rec", "sisumde", "simax", "t1pi0", "t2pi0" */};


  for ( std::string s : branch_names )
  {
    v_values[s] = new std::vector<double>;
    recons_tree->SetBranchAddress(s.c_str(), &v_values[s]);
  }

  // vrunnumber_ = new std::vector<int>;
  // format_tree->SetBranchAddress("runnumber", &vrunnumber_);

  vmaxposdetlayer_ = new std::vector<int>;
  recons_tree->SetBranchAddress("maxposdetlayer", &vmaxposdetlayer_);

  // vtime_ = new std::vector<unsigned>;
  // format_tree->SetBranchAddress("time", &vtime_);
  //
  // vflag_ = new std::vector<unsigned>;
  // format_tree->SetBranchAddress("flag", &vflag_);

  vmultihit_ = new std::vector<bool>;
  recons_tree->SetBranchAddress("multihit", &vmultihit_);

  vtrigger_ = new std::vector<bool>;
  recons_tree->SetBranchAddress("trigger", &vtrigger_);

  // vsisaturated_ = new std::vector<bool>;
  // recons_tree->SetBranchAddress("sisaturated", &vsisaturated_);
}


void RetrieveDataMitsuka::setupMontecarlo()
{
  std::string names[] = { "posx", "posy", "posz", "ekin", "px", "py", "pz"};

  if ( format_tree != nullptr )
  {
    for ( std::string s : names )
    {
      v_values[s] = new std::vector<double>;
      format_tree->SetBranchAddress(s.c_str(), &v_values[s]);
    }
    vcode_ = new std::vector<int>();
	  format_tree->SetBranchAddress("code", &vcode_);
  }
}


bool RetrieveDataMitsuka::retrieveEvent(int iEv)
{
  bool entry_flag = false;

  entry_flag = recons_tree->GetEntry(iEv);

  format_tree->GetEntry(iEv);

  if ( entry_flag )
  {
    for (int iT = 0; iT < 2; iT++)
    {
      //Get L90 and Energy
      pid[iT]    = v_values["pid"]->at(iT*4+1);
      energy[iT] = v_values["energy"]->at(iT*2);

      //Get Position
      float max_x, max_y;
      max_x = vmaxposdetlayer_->at(iT*2);
      max_y = vmaxposdetlayer_->at(iT*2+1);
      posx[iT] = v_values["vertex0"]->at(8*iT + 2*max_x);
      posy[iT] = v_values["vertex0"]->at(8*iT + 2*max_y + 1);

      float lor_amp_x = v_values["vertex1"]->at(8*iT + 2*max_x);
      float lor_amp_y = v_values["vertex1"]->at(8*iT + 2*max_y + 1);
      lor_amp[iT] = (lor_amp_x + lor_amp_y)/2;

      float lor_wdt_x = 0;
      float lor_wdt_y = 0;
      for ( std::string s : {"vertex2", "vertex3", "vertex4"} )
      {
        lor_wdt_x += v_values[s]->at(8*iT + 2*max_x);
        lor_wdt_y += v_values[s]->at(8*iT + 2*max_y + 1);
      }

      lor_wdt[iT] = (lor_wdt_x + lor_wdt_y)/2;

      x[iT] = -(posx[iT] - lhctodetx[iT] - beamcenter[0]);
      y[iT] = posy[iT] - lhctodety[iT] - beamcenter[1];

            //Get Multihit flag: 0 if not triggered, 1 for a single hit, 2 for multihit
      nhits[iT]  = (vtrigger_->at(iT)  ? 1 : 0)*(vmultihit_->at(iT)? 2 : 1);
    }
  }
  return entry_flag;
}



//Note: all lengths are measured in mm
MCEvent* RetrieveDataMitsuka::getMCdata()
{
    MCEvent* mce = new MCEvent;

    const double SiLayerDepth[4][2] = {
      {  1.99,     0.00},
      { 37.02,    35.04},
      {103.71,    69.33},
      {177.69, 139.76}
    };

    const double lhcf_to_layer0 = 46.66;
    double lhcx, lhcy, detx, dety;
    double lhcz = -1127;
    double depth_x = lhcf_to_layer0 + SiLayerDepth[0][0]; // depth of X silicon detector
    double depth_y = lhcf_to_layer0 + SiLayerDepth[0][1]; // depth of Y silicon detector
    int index1[2][2] = { {-1, -1}, {-1, -1}};


    if (vcode_ != nullptr)
    {
      int size = vcode_->size();
      //Use index to sort particles by energy_
      for (int iP = 0; iP < size; iP++)
      {
        //Convert position to mm and subtract offset
        lhcx = v_values["posx"]->at(iP)*10;
        lhcy = v_values["posy"]->at(iP)*10 + 20 ;

        // propagate true MC x,y to detector depth
        lhcx += (depth_x - lhcz) * v_values["px"]->at(iP) / v_values["pz"]->at(iP);
        lhcy += (depth_y - lhcz) * v_values["py"]->at(iP) / v_values["pz"]->at(iP);

        int hitTow = lhcy < 13.4 ? 0 : 1; //y=13.4 line separates the 2 towers
        detx = lhcx + lhctodetx[hitTow];
        dety = lhcy + lhctodety[hitTow];

        //if the particle actually hit the tower
        if ( 2 < detx && detx < 23+hitTow*7 &&
             2 < dety && dety < 23+hitTow*7)
        {
          if (index1[hitTow][0] < 0 )
          {
            index1[hitTow][0] = iP;
          }
          else if ( index1[hitTow][1] < 0 )
          {
            if (v_values["ekin"]->at(iP) > v_values["ekin"]->at(index1[hitTow][0]) )
            {
              index1[hitTow][1] = index1[hitTow][0];
              index1[hitTow][0] = iP;
            }
            else
            {
              index1[hitTow][1] = iP;
            }
          }
          else
          {
            bool set_ind = false;
            for (int sP=0; sP<N_PARTICLES && !set_ind; sP++)
            {
              if (v_values["ekin"]->at(iP) > v_values["ekin"]->at(index1[hitTow][sP]) )
              {
                for (int tP=N_PARTICLES-1; tP > sP; tP--)
                {
                  index1[hitTow][tP] = index1[hitTow][tP-1];
                }
                  index1[hitTow][sP] = iP;
                  set_ind = true;
                }
              }
            }
          }
        }



        for (int iT = 0; iT < N_TOWERS; iT++)
        {
          for (int iP = 0; iP < N_PARTICLES; iP++)
          {
            if (index1[iT][iP] >= 0)
            {

              mce->code[iT][iP] = vcode_->at(index1[iT][iP]);

              mce->p[0][iT][iP] = v_values["ekin"]->at(index1[iT][iP]);
              mce->p[1][iT][iP] = v_values["px"]->at(index1[iT][iP]);
              mce->p[2][iT][iP] = v_values["py"]->at(index1[iT][iP]);
              mce->p[3][iT][iP] = v_values["pz"]->at(index1[iT][iP]);

              mce->pos[0][iT][iP] = v_values["posx"]->at(index1[iT][iP])*10 - beamcenter[0];
              mce->pos[0][iT][iP] += (depth_x - lhcz) * v_values["px"]->at(index1[iT][iP]) / v_values["pz"]->at(index1[iT][iP]);
              mce->pos[1][iT][iP] = v_values["posy"]->at(index1[iT][iP])*10 + 20 - beamcenter[1];
              mce->pos[1][iT][iP] += (depth_y - lhcz) * v_values["py"]->at(index1[iT][iP]) / v_values["pz"]->at(index1[iT][iP]);
              mce->pos[2][iT][iP] = (depth_x + depth_y) / 2.;
            }
            else
            {
              mce->code[iT][iP] = 0;

              mce->p[0][iT][iP] = 0;
              mce->p[1][iT][iP] = 0;
              mce->p[2][iT][iP] = 0;
              mce->p[3][iT][iP] = 0;

              mce->pos[0][iT][iP] = 0;
              mce->pos[1][iT][iP] = 0;
              mce->pos[2][iT][iP] = 0;

            }
          }
        }
      }

      return mce;
}
