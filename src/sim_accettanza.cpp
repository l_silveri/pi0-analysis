#include <TLorentzVector.h>
#include <TGenPhaseSpace.h>
#include <TRandom3.h>
#include <TFile.h>
#include <TH2F.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TH1F.h>
#include "functions.h"

#define PI0_MASS    0.134977

extern Pi0Config conf;


bool isInRange(int i, double x, double y);
bool isSameTower(int tower, double x1, double y1, double x2, double y2);

void sim_accettanza()
{

    const double impact_distance = 141050;

    TLorentzVector Pi_pi0;
    Double_t masses [] = { 0, 0};
    TGenPhaseSpace event;
    TLorentzVector* Pf_gamma[2];
    Double_t posx[2], posy[2];
    double y, pt, phi;
    double px, py, pz, e;
    TRandom3 rand;

    //Define histograms
    TH3D* pos_histo_0 = new TH3D("pos_h_1", "position histogram 1", 200, -100, 100, 200, -100, 100, 40, 7, 15);
    TH3D* pos_histo_1 = new TH3D("pos_h_2", "position histogram 2", 200, -100, 100, 200, -100, 100, 40, 7, 15);
    TH3D* pos_histo_2 = new TH3D("pos_h_3", "position histogram 3", 200, -100, 100, 200, -100, 100, 40, 7, 15);
    TH1F* energy_hist = new TH1F("energy_h", "energy histogram", 500, 0, 5000);
    TH2F* y_pt_tot = new TH2F("y_pt_tot", "y_pt_tot", (15-7)/Y_MIN_BIN_STEP, 7, 15, (3-0)/PT_MIN_BIN_STEP, 0, 3);
    TH2F* y_pt_t1 = new TH2F("y_pt_t1", "y_pt tipo1", (15-7)/Y_MIN_BIN_STEP, 7, 15, (3-0)/PT_MIN_BIN_STEP, 0, 3); //1 fotone per torre
    TH2F* y_pt_t2 = new TH2F("y_pt_t2", "y_pt tipo2", (15-7)/Y_MIN_BIN_STEP, 7, 15, (3-0)/PT_MIN_BIN_STEP, 0, 3); //2 fotone nella stessa
    TH2F* pz_pt_tot = new TH2F("pz_pt_tot", "pz_pt_tot", 400, 0, 8000, 60, 0, 3);
    TH2F* pz_pt_t1 = new TH2F("pz_pt_t1", "pz_pt_t1", 400, 0, 8000, 60, 0, 3);
    TH2F* pz_pt_t2 = new TH2F("pz_pt_t2", "pz_pt_t2", 400, 0, 8000, 60, 0, 3);

    for (unsigned int i=0; i<1.e9; i++)
    {
        //generate random variables
        y = rand.Uniform(8, 12);
        pt = rand.Uniform(0, 1);
        phi = rand.Uniform(0, 2*M_PI);
        // pz = rand.Uniform(0,7000);

        //Get values with random generated variables
        px = pt*cos(phi);
        py = pt*sin(phi);
        e    = sqrt(pt*pt+PI0_MASS*PI0_MASS)/sqrt(1-tanh(y)*tanh(y));
        pz = e*tanh(y);
        Pi_pi0.SetPxPyPzE(px, py, pz, e);
        // Pi_pi0.SetXYZM(px, py, pz, PI0_MASS);
        // y = Pi_pi0.Rapidity();
        // e = Pi_pi0.Energy();

        //Fill histograms
        energy_hist->Fill(e);
        event.SetDecay(Pi_pi0, 2, masses);
        event.Generate();

        //Generate decay and get final position
        for (int j=0; j<2; j++)
        {
            Pf_gamma[j] = event.GetDecay(j);
            posx[j] = Pf_gamma[j]->Px()/Pf_gamma[j]->Pz()*impact_distance;
            posy[j] = Pf_gamma[j]->Py()/Pf_gamma[j]->Pz()*impact_distance;
            pos_histo_2->Fill(posx[j], posy[j], y);
        }

        //Fill the total events histograms
        y_pt_tot->Fill(y, pt);
        pz_pt_tot->Fill(pz, pt);

        //Controlla se l'evento è di tipo 2
        if ( isSameTower(0, posx[0], posy[0], posx[1], posy[1]) ||
                    isSameTower(1, posx[0], posy[0], posx[1], posy[1]))
        {
                y_pt_t2->Fill(y, pt);
                pz_pt_t2->Fill(pz, pt);
                pos_histo_0->Fill(posx[0], posy[0], y);
                pos_histo_0->Fill(posx[1], posy[1], y);
        }
        //Controlla se l'evento è di tipo 1
        else if ( (isInRange(0, posx[0], posy[0]) && isInRange(1, posx[1], posy[1])) ||
                            (isInRange(1, posx[0], posy[0]) && isInRange(0, posx[1], posy[1])))
        {
                y_pt_t1->Fill(y, pt);
                pz_pt_t1->Fill(pz, pt);
                pos_histo_1->Fill(posx[0], posy[0], y);
                pos_histo_1->Fill(posx[1], posy[1], y);
        }

    }

    TFile* outfile = new TFile(conf.getValue("acc_hst").c_str(), "RECREATE");
    pz_pt_t1->Write();
    pz_pt_t2->Write();
    pz_pt_tot->Write();
    y_pt_t1->Write();
    y_pt_t2->Write();
    y_pt_tot->Write();
    pos_histo_0->Write();
    pos_histo_1->Write();
    pos_histo_2->Write();
    outfile->Close();

}


//Check if the photon hit the tower
bool isInRange(int i, double x, double y)
{
    const double lhctodetx[] = { +8.0, +41.8};
    const double lhctodety[] = {+12.5, -14.3};
    const double beamcenterx = -3.488; // mm
    const double beamcentery = -2.706; // mm

    bool res_flag = false;
    double posx, posy;



    posx = x + lhctodetx[i] + beamcenterx;
    posy = y + lhctodety[i] + beamcentery;

    if ( posx > 2 && posx < 23+7*i && posy > 2 && posy < 23+7*i)
    {
        res_flag = true;
    }


    return res_flag;
}

//Check if 2 photons hit the same tower
bool isSameTower(int tower, double x1, double y1, double x2, double y2)
{
    bool res = false;

    if ( isInRange(tower, x1, y1) && isInRange(tower, x2, y2) )
    {
        res = true;
    }

    return res;
}
