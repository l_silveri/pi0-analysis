#include <iostream>
#include <fstream>
#include "Pi0Config.h"
#include "functions.h"
#include "RetrieveData.h"
#include "MCData.h"
#include "AcceptanceData.h"
#include <vector>
#include <algorithm>
#include <cmath>
#include <thread>
using namespace std;

#define _MONTECARLO         1
#define _RUN                0


#ifdef CXX17
    #include <filesystem>
    namespace fs = std::filesystem;
#else
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#endif

vector<string>& GetFilesinDirectory(string path, vector<string>& file_list);
Pi0Config conf;
float beamcenter_mc[2], beamcenter_data[2];


int main(int argc, char** argv)
{
    string confname;

    if (argc > 1)
    {
        confname = argv[1];
    }
    else
    {
        confname = "config.txt";
    }

    try
    {
        conf.parseFile(confname.c_str());
    }
    catch(FileNonExisting& fne)
    {
            cerr << "Problems opening file" << endl;
            exit(0);
    }

    try {
        beamcenter_mc[0] = stof(conf.getValue("beamcenterx_mc"));
        beamcenter_mc[1] = stof(conf.getValue("beamcentery_mc"));
    }
    catch (invalid_argument& iarg)
    {
        cerr << "Problem while setting montecarlo beamcenter, fallback to standard values" << endl;
        beamcenter_mc[0] = -3.272;
        beamcenter_mc[1] = -2.689-0.27;
    }
    try {
        beamcenter_data[0] = stof(conf.getValue("beamcenterx_data"));
        beamcenter_data[1] = stof(conf.getValue("beamcentery_data"));
    }
    catch (invalid_argument& iarg)
    {
        cerr << "Problem while setting data beamcenter, fallback to standard values" << endl;
        beamcenter_data[0] = -3.488;
        beamcenter_data[1] = -2.706;
    }

    if (conf.getValue("apply_cuts") == "on")
    {
        vector<string> McFileList;
        GetFilesinDirectory(conf.getValue("montecarlo_dir")+"/", McFileList);
        RetrieveData McData(McFileList);
        McData.setBeamCenter(beamcenter_mc[0], beamcenter_mc[1]);
        McData.setupMontecarlo();
        cutData(McData, _MONTECARLO);

        vector<string> RunFileList;
        GetFilesinDirectory(conf.getValue("run_dir")+"/", RunFileList);
        RetrieveData RunData(RunFileList);
        RunData.setBeamCenter(beamcenter_data[0], beamcenter_data[1]);
        cutData(RunData, _RUN);
    }

    if (conf.getValue("acceptance_sim") == "on")
    {
        sim_accettanza();
    }

    if (conf.getValue("montecarlo_analysis") == "on")
    {
        analyze_mc();
    }

    if (conf.getValue("data_analysis") != "off")
    {
        analyze();
    }


    return 0;

}

string float_to_str(float val, int dec_places)
{
    string num(to_string(val+0.5*pow(10, -dec_places)));
    size_t dot_pos = num.find('.');
    string res;

    if (dot_pos != string::npos )
        res = num.substr(0, dot_pos+dec_places+1);
    else
        res = num;

    return res;
}


vector<string>& GetFilesinDirectory(string path, vector<string>& file_list)
{
    for (const auto& entry : fs::directory_iterator(path) )
    {
        if ( entry.path().extension().string() == ".root")
        {
            string name = entry.path().filename().string();

            if ( name.find("cal_run") == string::npos)
            {
              file_list.push_back(path+entry.path().filename().string());
            }
        }
    }
    return file_list;
}
