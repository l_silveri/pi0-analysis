#include "Pi0Config.h"
#include <fstream>
#include <regex>
#include <algorithm>
#include <sstream>
using namespace std;

FileNonExisting file_ne;

Pi0Config::Pi0Config(const char* fname)
{
    parseFile(fname);
}


string Pi0Config::getValue(string key)
{
    return values[key];
}


void Pi0Config::parseFile(const char* fname)
{
    ifstream inp_file(fname);
    string row, row_s;
    regex comm("(#.*)|( )");
    regex val("(.*)=(.*)");
    cmatch match_res;

    if (!inp_file.good())
    {
        throw file_ne;
    }
    else
    {
        do {
            row_s = "";
            getline(inp_file, row);
            //Strip comments and spaces out
            regex_replace(back_inserter(row_s), row.begin(), row.end(), comm, string(""));
            //Find a match for key=value
            regex_match(row_s.c_str(), match_res, val);

            if (match_res.size() == 3)
            {
                //Passing key and value to map
                values[match_res[1]] = match_res[2];
            }
        } while(inp_file.good());
    }

    getBinning();
}


void Pi0Config::getBinning()
{
    float val;
    bool skip;

    for (string var : {"yBins", "ptBins", "acceptance_y_interval"} )
    {
        istringstream iss(this->values[var]);

        for (string token; getline(iss, token, ',');)
        {
            skip = false;

            try
            {
                val = stof(token);
            }
            catch (invalid_argument& arg)
            {
                skip = true;
            }
            if ( !skip )
            {
                this->binning[var].push_back(val);
            }
        }
        //sort(this->binning[var].begin(), this->binning[var].end());
    }
}

vector<float>& Pi0Config::getBinning(std::string varName)
{
    return this->binning[varName];
}
