#include "TH1D.h"
#include "TH2F.h"
#include "TH1F.h"
#include "AcceptanceData.h"
#include "TFile.h"
#include <cstring>
#include "functions.h"

AcceptanceData::AcceptanceData()
{
    sim_data_t[0] = NULL;
    sim_data_t[1] = NULL;
    evType = 0;
    sim_file_name = NULL;
}

AcceptanceData::AcceptanceData(const char* fname)
{
    sim_data_t[0] = NULL;
    sim_data_t[1] = NULL;
    evType = 0;
    sim_file_name = NULL;
    setFile(fname);
}

AcceptanceData::~AcceptanceData()
{
    if (sim_data_t[0] != NULL)
    {
        delete sim_data_t[0];
        delete sim_data_t[1];
    }
    if (sim_file_name != NULL)
    {
        delete sim_file_name;
    }
}

void AcceptanceData::setFile(const char* fname)
{
    if ( sim_file_name != NULL) delete [] sim_file_name;
    sim_file_name = new char[std::strlen(fname)];
    std::strcpy(sim_file_name, fname);

    updateHistos();
}

bool AcceptanceData::isSetFile()
{
    return this->sim_file_name;
}

/* Select 1 for Type-I event and 2 for Type-II events */
void AcceptanceData::selectEventType(int eventType)
{
    if (eventType == 1 || eventType == 2)
    {
        this->evType = eventType-1;
    }
}

void AcceptanceData::updateHistos()
{
    TFile* sim_file = TFile::Open(sim_file_name);

    if ( sim_data_t[0] != NULL)
    {
        delete sim_data_t[0];
        sim_data_t[0] = NULL;
        delete sim_data_t[1];
        sim_data_t[1] = NULL;
    }

    if ( sim_file != NULL)
    {
        sim_data_t[0] = (TH2F*) sim_file->Get("y_pt_t1");
        sim_data_t[1] = (TH2F*) sim_file->Get("y_pt_t2");
        sim_tot             = (TH2F*) sim_file->Get("y_pt_tot");
        sim_data_t[0]->SetDirectory(0);
        sim_data_t[1]->SetDirectory(0);
        sim_tot          ->SetDirectory(0);

        delete sim_file;

    }
}

float AcceptanceData::getValue(float y, float pt)
{
    float value_hit, value_tot;
    int gBin;
    std::vector<float> yBinning = conf.getBinning("yBins");
    std::vector<float> ptBinning = conf.getBinning("ptBins");

    float y_inf, y_sup;
    float pt_inf, pt_sup;

    float y_step = (yBinning[RANGE_MAX] - yBinning[RANGE_MIN]) / yBinning[N_BINS];
    for (y_sup = yBinning[RANGE_MIN]; y_sup < y; y_sup += y_step);
    y_inf = y_sup - y_step;

    float pt_step = (ptBinning[RANGE_MAX] - ptBinning[RANGE_MIN]) / ptBinning[N_BINS];
    for (pt_sup = ptBinning[RANGE_MIN]; pt_sup < pt; pt_sup += pt_step);
    pt_inf = pt_sup - pt_step;

    value_hit = value_tot = 0;

    for (float pt_ind = pt_inf; pt_ind < pt_sup; pt_ind+=PT_MIN_BIN_STEP)
    {
        for (float y_ind = y_inf; y_ind < y_sup; y_ind+=Y_MIN_BIN_STEP)
        {
            gBin = sim_data_t[evType]->FindBin(y_ind, pt_ind);
            value_hit +=    sim_data_t[evType]->GetBinContent(gBin);
            value_tot +=    sim_tot->GetBinContent(gBin);
        }
    }
    return value_hit/value_tot;
}

TH1F* AcceptanceData::getPtHistogram(float y, float pmin, float pmax)
{
    int yBin = sim_data_t[evType]->ProjectionX()->FindBin(y);
    TH1D* histo = sim_data_t[evType]->ProjectionY("", yBin, yBin);

    int start_bin = histo->FindBin(pmin);
    int stop_bin = histo->FindBin(pmax);
    int nBins = stop_bin-start_bin;

    TH1F* res_histo = new TH1F("pt_histo", "pt histo", nBins, pmin, pmax);

    for (int i=0; i<nBins; i++)
    {
            res_histo->SetBinContent(i, histo->GetBinContent(start_bin+i));
    }

    return res_histo;
}
