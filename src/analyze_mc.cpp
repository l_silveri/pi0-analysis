#include <iostream>
#include "RooFit.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooBifurGauss.h"
#include "RooAddPdf.h"
#include "RooChebychev.h"
#include "RooPolynomial.h"
#include "RooPlot.h"
#include "TH1F.h"
#include "TH2F.h"
#include <string>
#include "functions.h"
using namespace std;
using namespace RooFit;



//
//
// int main (int argc, char** argv)
// {
//  TApplication app("app", &argc, argv);
//
//  analyze_mc();
//
//  app.Run();
//  return 0;
// }
// #endif  // __GNUC__



void analyze_mc ()
{
    //Prende dati dal tree
    TFile* in_file = NULL;
    in_file = TFile::Open(conf.getValue("montecarlo_cut").c_str());
    cout << "Input file opened" << endl;
    if (in_file == NULL )
    {
        std::cerr << "File non trovato" << std::endl;
        std::exit(-1);
    }

    TTree* tree = (TTree*) in_file->Get("data_tree");

    RooRealVar mgg("mgg", "mgg", 0, 500, "MeV");
    RooRealVar mgg_sim("mgg_sim", "mgg_sim", 0, 500, "MeV");
    //mgg.setBins(250);
    // mgg_sim.setBins(125);

    RooRealVar pi_energy("pi_energy", "pi_energy", 0, 10.e4, "GeV");
    RooRealVar pi_energy_sim("pi_energy_sim", "pi_energy_sim", 0, 10.e4, "GeV");


    vector<float> y_binning = conf.getBinning("yBins");
    RooRealVar y("y", "rapidity", y_binning[RANGE_MIN], y_binning[RANGE_MAX]);
    RooRealVar y_sim("y_sim", "true rapidity", y_binning[RANGE_MIN], y_binning[RANGE_MAX]);
    y.setBins(y_binning[N_BINS]);
    y_sim.setBins(y_binning[N_BINS]);


    vector<float> pt_binning = conf.getBinning("ptBins");
    RooRealVar pt("pt", "p transverse", pt_binning[RANGE_MIN], pt_binning[RANGE_MAX], "GeV");
    RooRealVar pt_sim("pt_sim", "true p transverse", pt_binning[RANGE_MIN], pt_binning[RANGE_MAX], "GeV");
    pt.setBins(pt_binning[N_BINS]);
    pt_sim.setBins(pt_binning[N_BINS]);



    RooRealVar px("px", "p, x projection", 0, 1., "GeV");
    RooRealVar py("py", "p, y projection", 0, 1., "GeV");
    RooRealVar pz("pz", "p, z projection", 0, 10.e3, "GeV");

    RooRealVar px_sim("px_sim", "true p, x projection", 0, 1., "GeV");
    RooRealVar py_sim("py_sim", "true p, y projection", 0, 1., "GeV");
    RooRealVar pz_sim("pz_sim", "true p, z projection", 0, 10.e3, "GeV");

    RooRealVar l90_t1("pid1", "little tower's L90 parameter - L90 threshold", -44, 44, "X_0");
    RooRealVar l90_t2("pid2", "big tower's L90 parameter - L90 threshold", -44, 44, "X_0");

    RooRealVar ph_energy1("energy1", "energy of photon hitting little tower", 0, 10.e3, "GeV");
    RooRealVar ph_energy2("energy2", "energy of photon hitting big tower", 0, 10.e3, "GeV");

    RooRealVar code1("code1", "code of particle 1", 0, 20);
    RooRealVar code2("code2", "code of particle 2", 0, 20);
    RooRealVar twoph("twoph", "are there 2 photons", 0, 6);

//C'è da prendere i valori di ph_energy e l90
    RooDataSet data_rec("data_rec", "rec data from tree", tree, RooArgSet(mgg, y, pt, l90_t1, l90_t2, ph_energy1, ph_energy2, twoph, mgg_sim));
    RooDataSet data_true("data_true", "true data from tree", tree, RooArgSet(mgg_sim, y_sim, pt_sim, code1, code2));

    //Definizione chebychev
    RooRealVar p0("p0", "p0", 0., 5.), p1("p1", "p1", -5., 0.), p2("p2", "p2", 0., 5.);
    RooPolynomial cheby("cheby", "chebychev", mgg, RooArgList(p0, p1, p2));

    //Definizione gaussiana asimmetrica
    RooRealVar mu("mu", "mu", 1.40440e+02, 100, 180);
    RooRealVar sigmaL("sigmaL", "sigma left", 6, 2, 8);
    RooRealVar sigmaR("sigmaR", "sigma right", 6, 2, 8);
    RooBifurGauss gs("gauss", "gauss", mgg, mu, sigmaL, sigmaR);

    //Definizione somma delle 2 pdf
    RooRealVar fract("fract", "fraction of ", 0, 1);

    TH1F* sig_hist;
    TH1F* hadro_hist;
    TH1F* sib_hist;
    TH1F* mgg_hist;
    TH1F* bkg_hist;
    TH1F* mc_hist;
    TH1F* sig_pi_hist;
    TFile* out_file = new TFile(conf.getValue("montecarlo_hst").c_str(), "RECREATE");

    float energy_cut[2];
    try
    {
        energy_cut[0] = stof(conf.getValue("energy_ph_t1"));
    }
    catch (invalid_argument& arg)
    {
        cerr << "Cannot use indicated values for photon energy in small tower, fallback to 200 GeV";
        energy_cut[0] = 200;
    }

    try
    {
        energy_cut[1] = stof(conf.getValue("energy_ph_t2"));
    }
    catch (invalid_argument& arg)
    {
        cerr << "Cannot use indicated values for photon energy in large tower, fallback to 200 GeV";
        energy_cut[1] = 200;
    }

    ph_energy1.setRange("cut_rec", energy_cut[0], 100.e4);
    ph_energy2.setRange("cut_rec", energy_cut[1], 100.e4);
    l90_t1.setRange("cut_rec", -44, 0);
    l90_t2.setRange("cut_rec", -44, 0);

    code1.setRange("phRange", 0.9, 1.1);
    code2.setRange("phRange", 0.9, 1.1);
    twoph.setRange("pi0Range", 0.9, 1.1);
    mgg_sim.setRange("pi0Range", 134, 136);


    mgg.setRange("fitRange", 60, 220);
    string dname [] = { "_rec" , "_true" };

    TH2F* y_pt_histo_rec = new TH2F ("y_pt_rec", "y vs p_t for reconstructed data",
                            y_binning[N_BINS], y_binning[RANGE_MIN], y_binning[RANGE_MAX],
                            pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

    TH2F* y_pt_histo_nobkg = new TH2F ("y_pt_rec_pi", "y vs p_t for reconstructed pions",
                            y_binning[N_BINS], y_binning[RANGE_MIN], y_binning[RANGE_MAX],
                            pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

    TH2F* y_pt_histo_true = new TH2F ("y_pt_true", "y vs p_t for true data",
                             y_binning[N_BINS], y_binning[RANGE_MIN], y_binning[RANGE_MAX],
                             pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

    TH2F* y_pt_histo_all = new TH2F ("y_pt_all", "y vs p_t for sig+bkg",
                              y_binning[N_BINS], y_binning[RANGE_MIN], y_binning[RANGE_MAX],
                              pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);



    RooAbsData* data_reduced;
    RooAbsData* data_reduced_true;
    RooAbsData* bkg_data;
    RooAbsData* sig_data;
    RooAbsData* sig_pi_data;


    float y_step = ( y_binning[RANGE_MAX] - y_binning[RANGE_MIN] )/(y_binning[N_BINS]);

    //Fit gauss+chebychev
    for (int i=1; i<=y_binning[N_BINS]; i++)
    {
        float y_inf, y_sup;

        y_inf = y_binning[RANGE_MIN] + y_step*(i-1);
        y_sup = y_binning[RANGE_MIN] + y_step*i;

        string pdfname = "mgg_pdf_" + to_string(i);
        RooAddPdf pdf(pdfname.c_str(), "cheby + gauss", gs, cheby,fract);
        //Set background fit parameters to 0
        for (RooRealVar* param : {&p0, &p1, &p2, &fract } )
        {
            //param->setVal(1);
        }

        for (RooRealVar* param : {&sigmaL, &sigmaR} )
        {
            param->setVal(6);
        }


        cout << "Starting analysis of bin " << y_inf << " < y < " << y_sup << endl;

        y.setRange("y_range", y_inf, y_sup);
        y_sim.setRange("y_sim_range", y_inf, y_sup);


        data_reduced = data_rec.reduce(CutRange("cut_rec"))->reduce(CutRange("y_range"));


        pdf.fitTo(*data_reduced, Range("fitRange"));

        RooPlot* mggframe = mgg.frame(RooFit::Title(("mgg fit "+to_string(i)).c_str()));
        data_reduced->plotOn(mggframe);
        pdf.paramOn(mggframe);
        pdf.plotOn(mggframe);
        pdf.plotOn(mggframe, RooFit::Components("cheby"), RooFit::LineColor(kGreen+1));
        pdf.plotOn(mggframe, RooFit::Components("gauss"), RooFit::LineColor(kRed));

        //Limite sinistro (limitL) e destro (limitR) per le varie finestre spettrali
        //Finestra di segnale
        Double_t limitLs = mu.getValV() - 3*sigmaL.getValV();
        Double_t limitRs = mu.getValV() + 3*sigmaR.getValV();
        //Finestra di rumore sinistra
        Double_t limitLb_l = mu.getValV() - 7*sigmaL.getValV();
        Double_t limitRb_l = mu.getValV() - 4*sigmaL.getValV();
        //Finestra di rumore destra
        Double_t limitLb_r = mu.getValV() + 4*sigmaR.getValV();
        Double_t limitRb_r = mu.getValV() + 7*sigmaR.getValV();

        mgg.setRange("mgg_range_C", limitLs, limitRs);
        mgg.setRange("mgg_range_L", limitLb_l, limitRb_l);
        mgg.setRange("mgg_range_R", limitLb_r, limitRb_r);


        mgg_hist = new TH1F( ("mgg_hist_"+to_string(i)+dname[0]).c_str(),
                 ("mgg, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)).c_str(),
                 100, 0, 500);

        sib_hist = new TH1F( ("sb_hist_"+to_string(i)+dname[0]).c_str(),
                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                  " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        bkg_hist = new TH1F( ("bkg_hist_"+to_string(i)+dname[0]).c_str(),
                  ("bkg_pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                   " & "+float_to_str(limitLs) + "  mgg    " + float_to_str(limitRs)).c_str(),
                  pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        sig_hist = new TH1F( ("sig_hist_"+to_string(i)+dname[0]).c_str(),
                   ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                    " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        sig_pi_hist = new TH1F( ("sig_pi_hist_"+to_string(i)+dname[0]).c_str(),
                    ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)+
                     " & "+float_to_str(limitLs) + " < mgg < " + float_to_str(limitRs)).c_str(),
                     pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);


        RooAbsReal* bgl = cheby.createIntegral(mgg, Range("mgg_range_L"));
        RooAbsReal* bgr = cheby.createIntegral(mgg, Range("mgg_range_R"));
        RooAbsReal* bgc = cheby.createIntegral(mgg, Range("mgg_range_C"));
        Double_t R = bgc->getVal()/(bgl->getVal()+bgr->getVal());

        cout << "bgc is " << bgc->getVal() << endl << "bgl is " << bgl->getVal() << endl
                << "bgr is " << bgr->getVal() << endl;
        cout << "R param for y in (" << y_inf << ", " << y_sup << ") is " << R << endl;


        data_reduced->fillHistogram(mgg_hist, mgg);
        sig_data = data_reduced->reduce(CutRange("mgg_range_C"));
        bkg_data = data_reduced->reduce(CutRange("mgg_range_L"), CutRange("mgg_range_R"));

        sig_data->fillHistogram(sib_hist, pt);
        bkg_data->fillHistogram(bkg_hist, pt);
        sig_data->fillHistogram(sig_hist, pt);
        sig_pi_data = sig_data->reduce(CutRange("pi0Range"));
        sig_pi_data->fillHistogram(sig_pi_hist, pt);



        cout << "Background subtraction bin " << y_inf << " < y < " << y_sup << endl;

        sig_hist->Add(bkg_hist, -R);

        for (int j=1; j<pt_binning[N_BINS]; j++)
        {
            sig_hist->SetBinError(i, sqrt(sib_hist->GetBinContent(i) + R*R*bkg_hist->GetBinContent(i)));
        }

        cout << "Done analysis of bin " << y_inf << " < y < " << y_sup << endl;
        mggframe->Write();
        mgg_hist->Write();
        sib_hist->Write();
        bkg_hist->Write();
        sig_hist->Write();
        sig_pi_hist->Write();
        out_file->Flush();

        cout << "Getting MC Truth data for bin " << y_inf << " < y < " << y_sup << endl;



        data_reduced_true = data_true.reduce(CutRange("pi0Range"))->reduce(CutRange("y_sim_range"));


        mc_hist = new TH1F( ("sig_hist_"+to_string(i)+dname[1]).c_str(),
                                                 ("pt, " + float_to_str(y_inf) + " < y < " + float_to_str(y_sup)).c_str(),
                                                    pt_binning[N_BINS], pt_binning[RANGE_MIN], pt_binning[RANGE_MAX]);

        data_reduced_true->fillHistogram(mc_hist, pt_sim);

        for (int j=1; j<=pt_binning[N_BINS]; j++)
        {
            y_pt_histo_nobkg->SetBinContent(i, j, sig_pi_hist->GetBinContent(j));
            y_pt_histo_nobkg->SetBinError(i, j, sig_pi_hist->GetBinError(j));
            y_pt_histo_rec->SetBinContent(i, j, sig_hist->GetBinContent(j));
            y_pt_histo_rec->SetBinError(i, j, sig_hist->GetBinError(j));
            y_pt_histo_true->SetBinContent(i, j, mc_hist->GetBinContent(j));
            y_pt_histo_true->SetBinError(i, j, mc_hist->GetBinError(j));
            y_pt_histo_all->SetBinContent(i, j, sib_hist->GetBinContent(j));
            y_pt_histo_all->SetBinError(i, j, sib_hist->GetBinError(j));
        }


        mc_hist->Write();
        out_file->Flush();
    }
    y_pt_histo_nobkg->Write();
    y_pt_histo_rec->Write();
    y_pt_histo_true->Write();
    y_pt_histo_all->Write();
    in_file->Close();
    out_file->Close();

}
