#include "RetrieveData.h"
#include "AcceptanceData.h"
#include <vector>
#include <string>
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TH2F.h"
#include "functions.h"
using namespace std;


// void processMCEvent(RetrieveData& dt);
double L90Boundary(int tower, double energy, int mode);



void cutData(RetrieveDataAbs& data, bool isMontecarlo)
{
	int iEv=0;
	double x[2], y[2];
	const double impact_distance = 141050;
	//Two for each variable: first for "real", second for measured values
	double mgg[2], pi_energy[2], y_rap[2], px[2], py[2], pz[2], pt[2], theta[2], eta[2], pt_m_fact, p_mod, id[2];
	double len[2];
	int code1=0, code2=0;
	int two_ph;
	double energy1, energy2, energy1_1, energy2_1;
	MCEvent* mce;
	bool hit, mc_hit, trig[2];
  int l90_sel_eff_mode;

	bool use_auto_l90[2] = {false, false};
	float l90_cuts[2], energy_cut[2], ph_e_fact, border_size;
  float z_over_d[2][2];


	TFile* s_file;

	if (isMontecarlo)
	{
		s_file = new TFile(conf.getValue("montecarlo_cut").c_str(),"RECREATE");
	}
	else
	{
		s_file = new TFile(conf.getValue("run_cut").c_str(),"RECREATE");
	}

	TTree* data_tree = new TTree("data_tree", "data tree");
  TTree* unfolding_tree = new TTree("unfolding_tree", "unfolding tree");
	TTree* l90_vs_lorentz = new TTree("l90_vs_lorentz", "l90 vs lorentz");
	TH2F* l90_hist[2];
	TH2F* l90_hist_t[2];

	l90_hist[0] = new TH2F("l90_h_1", "l90 hist 1", 450, 0, 45, 700, 0, 7000);
	l90_hist[1] = new TH2F("l90_h_2", "l90 hist 2", 450, 0, 45, 700, 0, 7000);
	l90_hist_t[0] = new TH2F("l90_h_1_t", "l90 hist 1 tr", 450, 0, 45, 700, 0, 7000);
	l90_hist_t[1] = new TH2F("l90_h_2_t", "l90 hist 2 tr", 450, 0, 45, 700, 0, 7000);

	try
	{
		energy_cut[0] = stof(conf.getValue("energy_ph_t1"));
	}
	catch (invalid_argument& arg)
	{
		cerr << "Cannot use indicated values for photon energy in small tower, fallback to 200 GeV";
		energy_cut[0] = 200;
	}

	try
	{
		energy_cut[1] = stof(conf.getValue("energy_ph_t2"));
	}
	catch (invalid_argument& arg)
	{
		cerr << "Cannot use indicated values for photon energy in large tower, fallback to 200 GeV";
		energy_cut[1] = 200;
	}

	if (conf.getValue("l90_cut_t1") == "auto" )
	{
		use_auto_l90[0] = true;
	}
	else
	{
		try
		{
			l90_cuts[0] = stof(conf.getValue("l90_cut_t1"));
		}
		catch (invalid_argument& arg)
		{
			cerr << "Cannot use indicated values for L90 cuts in small tower, fallback to auto" << endl;
			use_auto_l90[0] = true;
		}
	}

	if (conf.getValue("l90_cut_t2") == "auto" )
	{
		use_auto_l90[1] = true;
	}
	else
	{
		try
		{
			l90_cuts[1] = stof(conf.getValue("l90_cut_t2"));
		}
		catch (invalid_argument& arg)
		{
			cerr << "Cannot use indicated values for L90 cuts in large tower, fallback to auto" << endl;
			use_auto_l90[1] = true;
		}
	}

	try
	{
		ph_e_fact = stof(conf.getValue("ph_energy_factor"));
	}
	catch (invalid_argument& arg)
	{
		cerr << "Cannot use indicated values for energy photon multiplier, fallback to 1" << endl;
		ph_e_fact = 1;
	}

	try
	{
		border_size = stof(conf.getValue("border_size"));
	}
	catch (invalid_argument& arg)
	{
		cerr << "Cannot use indicated values for energy photon multiplier, fallback to 2mm" << endl;
		border_size = 2;
	}

  try
  {
    l90_sel_eff_mode = stoi(conf.getValue("l90_sel_eff_mode"));
  }
  catch (invalid_argument& arg)
  {
    l90_sel_eff_mode = 0;
  }


	//Branches for reconstruction
	data_tree->Branch("mgg", &mgg[0]);
	data_tree->Branch("pi_energy", &pi_energy[0]);
	data_tree->Branch("y", &y_rap[0]);
	data_tree->Branch("px", &px[0]);
	data_tree->Branch("py", &py[0]);
	data_tree->Branch("pz", &pz[0]);
	data_tree->Branch("pt", &pt[0]);
	data_tree->Branch("pid1", &id[0]);
	data_tree->Branch("pid2", &id[1]);
	data_tree->Branch("l90_tow1", &data.pid[0]);
	data_tree->Branch("l90_tow2", &data.pid[1]);

	//Branches for montecarlo truth
	data_tree->Branch("pi_energy_sim", &pi_energy[1]);
	data_tree->Branch("y_sim", &y_rap[1]);
	data_tree->Branch("px_sim", &px[1]);
	data_tree->Branch("py_sim", &py[1]);
	data_tree->Branch("pz_sim", &pz[1]);
	data_tree->Branch("pt_sim", &pt[1]);
	data_tree->Branch("mgg_sim", &mgg[1]);
	data_tree->Branch("code1", &code1);
	data_tree->Branch("code2", &code2);
	data_tree->Branch("twoph", &two_ph);
	data_tree->Branch("hit", &hit);

	//Branches for Lorentzian
	l90_vs_lorentz->Branch("l90_tow1", &data.pid[0]);
	l90_vs_lorentz->Branch("l90_tow2", &data.pid[1]);
	l90_vs_lorentz->Branch("amp_tow1", &data.lor_amp[0]);
	l90_vs_lorentz->Branch("amp_tow2", &data.lor_amp[1]);
	l90_vs_lorentz->Branch("wdt_tow1", &data.lor_wdt[0]);
	l90_vs_lorentz->Branch("wdt_tow2", &data.lor_wdt[1]);
	l90_vs_lorentz->Branch("code1", &code1);
	l90_vs_lorentz->Branch("code2", &code2);
	l90_vs_lorentz->Branch("energy1", &data.energy[0]);
	l90_vs_lorentz->Branch("energy2", &data.energy[1]);
	l90_vs_lorentz->Branch("trig1", &trig[0]);
	l90_vs_lorentz->Branch("trig2", &trig[1]);

  //Branches for unfolding
  unfolding_tree->Branch("energy1_obs", &data.energy[0]);
  unfolding_tree->Branch("energy2_obs", &data.energy[1]);
  unfolding_tree->Branch("alpha1_obs", z_over_d[0][0]);
  unfolding_tree->Branch("alpha2_obs", z_over_d[0][1]);
  unfolding_tree->Branch("energy1_sim", energy1);
  unfolding_tree->Branch("energy2_sim", energy2);
  unfolding_tree->Branch("alpha1_sim", z_over_d[1][0]);
  unfolding_tree->Branch("alpha2_sim", z_over_d[1][1]);


	while (data.retrieveEvent(iEv))
	{
	 /*
		* Select type-I events with minimum energy per photon of 200GeV, which
		* don't fall within 2mm borders and lose 90% of the energy within 20X0
		*/
		if ( isMontecarlo)
		{
			mce = data.getMCdata();
			mc_hit = (mce->code[0][0] == 1 && mce->code[1][0] == 1);
			code1 = mce->code[0][0];
			code2 = mce->code[1][0];
			two_ph = ( code1 == 1 ? code2 : 0);
		}
		else
		{
			mc_hit = false;
			data.energy[0] *= ph_e_fact;
			data.energy[1] *= ph_e_fact;
		}

		hit = true;

		for (int i=0; i<2; i++)
		{
			if (
				data.nhits[i] == 1 &&
				data.energy[i] > energy_cut[i] &&
				//i*7 takes into account 7mm larger tower
				data.posx[i] > border_size	&& data.posx[i] < 25 - border_size + i*7 &&
				data.posy[i] > border_size	&& data.posy[i] < 25 - border_size + i*7
			)
			{
				l90_hist[i]->Fill(data.pid[i], data.energy[i]);
				trig[i] = true;

				if ( mce->code[i][0] == 1 )
				{
					l90_hist_t[i]->Fill(data.pid[i], data.energy[i]);
				}
			}
			else
			{
				trig[i] = false;
				hit = false;
			}
		}

		l90_vs_lorentz->Fill();

		// hit = (
		//			data.nhits[0] == 1	 && data.nhits[1] == 1		&&
		//			data.posx[0] > 2			&& data.posx[0] < 23			&&
		//			data.posy[0] > 2			&& data.posy[0] < 23			&&
		//			data.posx[1] > 2		 && data.posx[1] < 30			&&
		//			data.posy[1] > 2		 && data.posy[1] < 30
		//			// data.pid[0] < 20.		&& data.pid[1] < 20.
		// );

		if (hit || mc_hit )
		{

			// Simulated results
			theta[0] = sqrt( pow(data.x[0]-data.x[1], 2) + pow(data.y[0]-data.y[1], 2))/impact_distance;
			mgg[0] = sqrt( data.energy[0]*data.energy[1] )*theta[0]*1000;
			pi_energy[0] = data.energy[0]+data.energy[1];

			len[0] = sqrt(data.x[0]*data.x[0]+data.y[0]*data.y[0]+impact_distance*impact_distance);
			len[1] = sqrt(data.x[1]*data.x[1]+data.y[1]*data.y[1]+impact_distance*impact_distance);

			px[0] = data.energy[0]*data.x[0]/len[0] + data.energy[1]*data.x[1]/len[1];
			py[0] = data.energy[0]*data.y[0]/len[0] + data.energy[1]*data.y[1]/len[1];
			pz[0] = data.energy[0]*impact_distance/len[0] + data.energy[1]*impact_distance/len[1];
			energy1_1 = data.energy[0];
			energy2_1 = data.energy[1];
      z_over_d[0][0] = impact_distance/len[0];
      z_over_d[0][1] = impact_distance/len[1];


			if (isMontecarlo && mce != NULL)
			{
				// Montecarlo truth
				len[0] = sqrt(mce->pos[0][0][0]*mce->pos[0][0][0]+mce->pos[1][0][0]*mce->pos[1][0][0]+impact_distance*impact_distance);
				len[1] = sqrt(mce->pos[0][1][0]*mce->pos[0][1][0]+mce->pos[1][1][0]*mce->pos[1][1][0]+impact_distance*impact_distance);

				pi_energy[1] = mce->p[0][0][0] + mce->p[0][1][0];
				px[1] = mce->p[0][0][0]*mce->pos[0][0][0]/len[0] + mce->p[0][1][0]*mce->pos[0][1][0]/len[1];
				py[1] = mce->p[0][0][0]*mce->pos[1][0][0]/len[0] + mce->p[0][1][0]*mce->pos[1][1][0]/len[1];
				pz[1] = mce->p[0][0][0]*impact_distance/len[0] + mce->p[0][1][0]*impact_distance/len[1];
        z_over_d[1][0] = impact_distance/len[0];
        z_over_d[1][1] = impact_distance/len[1];
				energy1 = mce->p[0][0][0];
				energy2 = mce->p[0][1][0];

        for (int i=0; i<2; i++)
        {
          if (!(
            mce->p[0][i][0] > energy_cut[i] &&
            //i*7 takes into account 7mm larger tower
            mce->pos[0][i][0] > border_size	&& mce->pos[0][i][0] < 25 - border_size + i*7 &&
            mce->pos[1][i][0] > border_size	&& mce->pos[1][i][0] < 25 - border_size + i*7
          ))
          {
              mc_hit = false;
          }
        }
				theta[1] = sqrt( pow(mce->pos[0][0][0] - mce->pos[0][1][0], 2) + pow(mce->pos[1][0][0] - mce->pos[1][1][0], 2))/impact_distance;
				mgg[1] = sqrt( mce->p[0][0][0]*mce->p[0][1][0] )*theta[1]*1000;
				delete mce;
				mce = NULL;
			}

			for (int i=0; i<2; i++)
			{
				id[i] = data.pid[i] - (use_auto_l90[i] ? L90Boundary(i, data.energy[i], l90_sel_eff_mode) : l90_cuts[i]);
			}

			if ( isMontecarlo &&	(hit || ( mc_hit && mgg[1] > 133 && mgg[1] < 136) ))
			{
				for (int i=0; i<2; i++)
				{
					pt[i] = sqrt(px[i]*px[i] + py[i]*py[i]);
					y_rap[i] = 0.5*log((pi_energy[i]+pz[i])/(pi_energy[i]-pz[i]));

				}
				data_tree->Fill();
        unfolding_tree->Fill();
			}
			else if ( id[0] < 0 && id[1] < 0 &&
				data.energy[0] > energy_cut[0] && data.energy[1] > energy_cut[1] )
			{
				pt[0] = sqrt(px[0]*px[0] + py[0]*py[0]);
				y_rap[0] = 0.5*log((pi_energy[0]+pz[0])/(pi_energy[0]-pz[0]));
				data_tree->Fill();
			}

		}

		iEv++;
	}

	data_tree->Write();
	l90_hist[0]->Write();
	l90_hist[1]->Write();
	l90_vs_lorentz->Write();

	if (isMontecarlo)
	{
		l90_hist_t[0]->Write();
		l90_hist_t[1]->Write();
    unfolding_tree->Write();
	}

	s_file->Close();

}




/* Values for mode are based on single photon selection efficiency threshold:
 * 0 - 90%
 * 1 - 85%
 * 2 - 95%
 * other - taken as 90%
 */
double L90Boundary(int tower, double energy, int mode)
{
    double* p0;
    double* p1;
    double* p2;

    //Values for 90% selection efficiency threshold
    double p0_90[] = {4.46e+00, 1.36e+01};
    double p1_90[] = {1.10e-02, 1.86e-04};
    double p2_90[] = {6.33e+01, 3.97e+00};

    // //Values for 85% selection efficiency threshold

    double p0_85[] = {4.41e+00, 1.40e+01};
    double p1_85[] = {1.02e-02, 1.72e-04};
    double p2_85[] = {5.75e+01, 3.63e+00};

    // //Values for 95% selection efficiency threshold

    double p0_95[] = {4.42e+00, 1.81e+01};
    double p1_95[] = {1.28e-02, 1.02e-04};
    double p2_95[] = {8.44e+01, 2.95e+00};


    switch (mode) {
      case 1:
        p0 = p0_85;
        p1 = p1_85;
        p2 = p2_85;
        break;
      case 2:
        p0 = p0_95;
        p1 = p1_95;
        p2 = p2_95;
        break;
      default:
        p0 = p0_90;
        p1 = p1_90;
        p2 = p2_90;
    }

    return p0[tower] * log(p1[tower] * energy + p2[tower]);

}
