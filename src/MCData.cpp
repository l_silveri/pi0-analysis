#include "TH2F.h"
#include "TFile.h"
#include "MCData.h"
#include <cstring>

MCData::MCData()
{
    sim_histo_rec = NULL;
    mc_fname = NULL;
}


MCData::MCData(const char* fname)
{
    sim_histo_rec = NULL;
    mc_fname = NULL;
    setFile(fname);
}


MCData::~MCData()
{
    if (sim_histo_rec != NULL )
    {
        delete sim_histo_rec;
    }
    if (mc_fname != NULL)
    {
        delete mc_fname;
    }
}


void MCData::setFile(const char* fname)
{
    if ( mc_fname != NULL) delete [] mc_fname;
    mc_fname = new char[30];//[std::strlen(fname)];
    std::strcpy(mc_fname, fname);

    updateHistos();
}


bool MCData::isSetFile()
{
    return (bool)this->mc_fname;
}


void MCData::updateHistos()
{
    TFile* mc_file = TFile::Open(mc_fname);

    if (sim_histo_rec != NULL )
    {
        delete sim_histo_rec;
        sim_histo_rec = NULL;
    }

    if (mc_file != NULL)
    {
        sim_histo_rec = (TH2F*) mc_file->Get("y_pt_rec");
        sim_histo_true = (TH2F*) mc_file->Get("y_pt_true");
        sim_histo_recpi = (TH2F*) mc_file->Get("y_pt_rec_pi");
        sim_histo_all = (TH2F*) mc_file->Get("y_pt_all");
        sim_histo_rec->SetDirectory(0);
        sim_histo_true->SetDirectory(0);
        sim_histo_recpi->SetDirectory(0);
        sim_histo_all->SetDirectory(0);

        delete mc_file;
    }
}

float MCData::getValue(float y, float pt)
{
    float retVal = 0;

    if ( sim_histo_rec != NULL)
    {
        int gBin_rec =  sim_histo_recpi->FindBin(y, pt);
        retVal = sim_histo_recpi->GetBinContent(gBin_rec)/sim_histo_true->GetBinContent(gBin_rec);
    }

    return retVal;
}

float MCData::getCorrectionError(float y, float pt)
{
    float retVal = 0;

    if ( sim_histo_rec != NULL)
    {
      int gBin_rec = sim_histo_recpi->FindBin(y, pt);
      retVal = this->getValue(y, pt)*sqrt( 1/sim_histo_recpi->GetBinContent(gBin_rec)
                            + 1/sim_histo_true->GetBinContent(gBin_rec) );
    }
    return retVal;
}


float MCData::getBkgCorrection(float y, float pt)
{
  float retVal = 0;

  if ( sim_histo_rec != NULL)
  {
      int gBin_rec =  sim_histo_rec->FindBin(y, pt);

      float n_ph = sim_histo_recpi->GetBinContent(gBin_rec);
      float n_all = sim_histo_all->GetBinContent(gBin_rec);
      float n_sig = sim_histo_rec->GetBinContent(gBin_rec);
      if (n_all != n_sig)
      {
        retVal = ( n_all - n_ph ) / (n_all - n_sig);
      }
  }

  return retVal;
}
